Welcome to the documentation of the [HH inference tools](https://gitlab.cern.ch/hh/tools/inference).

Read the [Introduction](introduction.md) for the setup instructions of this repository.
More detailed information on the different scans & fits be found in the following sections:

- [Datacard Combination](tasks/combinedatacards.md)
- [Text to Workspace](tasks/t2w.md)
- [Upper Limits](tasks/limits.md)
- [1D Likelihood Scans](tasks/likelihood1d.md)
- [2D Likelihood Scans](tasks/likelihood2d.md)
- [Pulls and Impacts](tasks/pullsandimpacts.md)
- [Significances](tasks/significances.md)
- [Miscellaneous tasks](tasks/misc.md)

An experimental interactive datacard viewer exists too (thanks to [Benjamin Fischer](https://git.rwth-aachen.de/3pia/cms_analyses/common/-/blob/master/view_datacard.html)).
